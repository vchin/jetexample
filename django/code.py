
class ExSearchQuery(SearchQuery):
    def as_sql(self, compiler, connection):
        params = [self.value]
        if self.config:
            config_sql, config_params = compiler.compile(self.config)
            template = 'to_tsquery({}::regconfig, %s)'.format(config_sql)
            params = config_params + [self.value]
        else:
            template = 'to_tsquery(%s)'
        if self.invert:
            template = '!!({})'.format(template)
        return template, params


class SearchView(BaseMixin, ListView):
    extra_param_list = ['shop_id', 'order_by', 'term']

    def get_breadcrumbs(self):
        return self.breadcrumbs + [{'title': 'Поиск'}]

    def get_paginate_by(self, queryset):
        return core_models.SiteSettings.val_get('paginate_by', 10)

    def get_queryset(self):
        try:
            return list(self.products)
        except:
            return list()

    def dispatch(self, request, *args, **kwargs):
        self.curr_cat = None
        self.original_term = re.sub('[\s\t]+', ' ',
            request.GET.get('term', '').strip())
        self.original_term = re.sub('[^a-zA-Z0-9_\sа-яА-Я]', '',
            self.original_term).strip()
        self.term = re.sub(' +', '&', self.original_term)
        self.category_slug = kwargs.get('category_slug', '').strip()

        vector = SearchVector(
            'title', weight='A', config='russian'
        ) + SearchVector(
            'device_model__title',
            weight='C',
            config='russian'
        ) + SearchVector(
            'device_model__vendor__title',
            weigth='B',
            config='russian'
        )
        # vector = SearchVector('title', 'device_model__title',
        #                       'device_model__vendor__title',
        #                       config='russian')

        query = ExSearchQuery('{}:*'.format(self.term),
            config='russian')
        qset = core_models.Product.objects.annotate(
            search=vector, rank=SearchRank(vector, query),
        )

        if self.term:
            qset = qset.filter(search=query).order_by('-rank')
        else:
            return super(SearchView, self).dispatch(request, *args, **kwargs)

        ids = qset.values_list('id', flat=True).distinct()
        ids = list(ids)
        self.num_prods = len(set(ids))

        self.cats = core_models.Category.objects.filter(product__in=ids,
            level__gt=1).annotate(
            num_prod=Count('product', distinct=True)
        )

        if self.category_slug:
            self.curr_cat = core_models.Category.objects.filter(
                slug=self.category_slug
            ).first()
        # elif self.num_prods and self.num_prods > 0:
        else:
            self.curr_cat = None

        products = OrderedDict()

        if self.curr_cat:
            qset = qset.filter(category=self.curr_cat)
            ids = qset.values_list('id', flat=True).distinct()

        # preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)]) if ids else 'title'
        objs = core_models.Product.objects.filter(
            id__in=ids
        ).select_related('category').prefetch_related(
            'productproperty_set',
            'productimage_set'
        )
        objs = dict([(obj.id, obj) for obj in objs])
        sorted_objects = [objs[id] for id in ids]
        for product in sorted_objects:
            products[product.pk] = {
                'product': product,
                'product_shop_list': []
            }

        qset_ps = core_models.ProductShop.objects.filter(
            product_id__in=products.keys(),
            enabled=True,
            shop__enabled=True).select_related('shop')

        if request.curr_city:
            shop_ids = core_models.ShopAddress.objects.filter(
                city=request.curr_city, shop__enabled=True).values_list(
                'shop_id',
                flat=True).distinct()
            qset_ps = qset_ps.filter(shop__in=shop_ids,
                city=request.curr_city)

        pspp = core_models.SiteSettings.val_get('product_shop_any_num')
        # TODO: need optimisation
        # https://stackoverflow.com/questions/25060663/postgresql-window-function-with-limit

        for product_shop in qset_ps:
            if len(products[product_shop.product_id][
                'product_shop_list']) == pspp:
                continue
            products[product_shop.product_id]['product_shop_list'].append(
                product_shop)


        self.products = products.values()

        return super(SearchView, self).dispatch(request, *args, **kwargs)


###############
USAGE:
# class _Form(ExModelForm):
# rx_attrs = {'username': {'placeholder': 'user name', 'class' : 'class name'}}

# adds placeholder from from model or rx_atrrs

class ExModelForm(forms.ModelForm):
    rx_settings = {'placeholder': {
        'from_label': True,
        'from_verbose': True
    }}

    rx_attrs = {}

    def __init__(self, *args, **kwargs):
        super(ExModelForm, self).__init__(*args, **kwargs)

        # for field in self.instance._meta
        if 'placeholder' in self.rx_settings and (
                self.rx_settings['placeholder'].get('from_label') or self.rx_settings['placeholder'].get(
            'from_verbose')):

            for form_key, form_field in self.fields.iteritems():
                placeholder = None

                if self.rx_settings['placeholder'].get('from_label') and hasattr(self.instance, form_key):
                    placeholder = self.instance._meta.get_field_by_name(form_key)[0].verbose_name.title()
                elif self.rx_settings['placeholder'].get('from_verbose') and self.fields[form_key].label:
                    placeholder = self.fields[form_key].label

                if not placeholder is None:
                    self.fields[form_key].widget.attrs['placeholder'] = placeholder
                    self.fields[form_key].widget.attrs['data-placeholder'] = placeholder

        for key, val in self.rx_attrs.iteritems():
            if key in self.fields:
                self.fields[key].widget.attrs.update(val)


class ExForm(forms.Form):
    rx_attrs = {}
    rx_settings = {'placeholder': {
        'from_label': True,
        'from_verbose': True
    }}

    def __init__(self, *args, **kwargs):
        super(ExForm, self).__init__(*args, **kwargs)

        for form_key, form_field in self.fields.iteritems():
            self.fields[form_key].widget.attrs['placeholder'] = \
                self.fields[form_key].label

        for key, val in self.rx_attrs.iteritems():
            if key in self.fields:
                self.fields[key].widget.attrs.update(val)






#############################
