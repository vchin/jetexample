DO $$
BEGIN
  CREATE OR REPLACE FUNCTION public.history (
  )
  RETURNS trigger AS
  $body$
  DECLARE
    tbl_name TEXT := TG_ARGV[0];
    tbl_fields TEXT := TG_ARGV[1];
    tbl_values TEXT := TG_ARGV[2];
  BEGIN
    IF (TG_OP = 'UPDATE') THEN
      EXECUTE format('INSERT INTO %I(%s) VALUES (%s)', TG_TABLE_NAME || tbl_name, tbl_fields, tbl_values) USING OLD, NEW;
      RETURN NEW;
    END IF;
  EXCEPTION
  WHEN unique_violation THEN
  --  statements;
  END;
  $body$
  LANGUAGE 'plpgsql'
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  COST 100;

  CREATE OR REPLACE FUNCTION public.history_hstore (
  )
  RETURNS trigger AS
  $body$
    DECLARE
      tbl_name TEXT := TG_ARGV[0];
      tbl_fields TEXT := TG_ARGV[1];
      tbl_values TEXT := TG_ARGV[2];
      tbl_fcheck TEXT[] := TG_ARGV[3];
      old_values hstore;
      new_values hstore;
      old_changes hstore;
      new_changes hstore;
    BEGIN
      IF (TG_OP = 'UPDATE') THEN
        old_values := slice(hstore(OLD), tbl_fcheck);
        new_values := slice(hstore(NEW), tbl_fcheck);
        old_changes := old_values - new_values;
        new_changes := new_values - old_values;

        IF old_changes != ''::hstore OR new_changes != ''::hstore THEN
          EXECUTE format(
            'INSERT INTO %I (%s, old_values, new_values) '
            'VALUES (%s, $3, $4)',
              TG_TABLE_NAME || tbl_name,
              tbl_fields, tbl_values
          ) USING OLD, NEW, old_changes, new_changes;
        END IF;

        RETURN NEW;
      END IF;
    EXCEPTION
    WHEN unique_violation THEN
    --  statements;
    END;
  $body$
  LANGUAGE 'plpgsql'
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  COST 100;

--   CREATE OR REPLACE FUNCTION public.history_nullif_old (
--   )
--   RETURNS trigger AS
--   $body$
--   DECLARE
--     tbl_name TEXT := TG_ARGV[0];
--     tbl_fields TEXT := TG_ARGV[1];
--     tbl_values TEXT := TG_ARGV[2];
--     tbl_nullif TEXT[] := TG_ARGV[3];
--     tbl_fields_nullif TEXT;
--     tbl_values_nullif TEXT;
--   BEGIN
--     IF (TG_OP = 'UPDATE') THEN
--       SELECT ARRAY_TO_STRING(tbl_nullif, ', ') INTO tbl_fields_nullif;
--       SELECT ARRAY_TO_STRING(ARRAY_AGG(FORMAT('NULLIF($1.%s, $2.%s)', v, v)), ', ') INTO tbl_values_nullif FROM UNNEST(tbl_nullif) v;
--
--       EXECUTE format(
--         'WITH nw (%s) AS ('
--         '  VALUES (%s)'
--         ') '
--         'INSERT INTO %I (%s,%s) '
--         'SELECT %s, * FROM nw '
--         'WHERE NOT(ROW((nw).*) IS NULL)',
--           tbl_fields_nullif,
--           tbl_values_nullif,
--           TG_TABLE_NAME || tbl_name, tbl_fields, tbl_fields_nullif,
--           tbl_values
--       ) USING OLD, NEW;
--
--       RETURN NEW;
--     END IF;
--   EXCEPTION
--   WHEN unique_violation THEN
--   --  statements;
--   END;
--   $body$
--   LANGUAGE 'plpgsql'
--   VOLATILE
--   CALLED ON NULL INPUT
--   SECURITY INVOKER
--   COST 100;

--   CREATE TRIGGER order_statushistory
--   BEFORE UPDATE OF api_status
--   ON public.orders_order FOR EACH ROW
--   WHEN (OLD.api_status IS DISTINCT FROM NEW.api_status)
--   EXECUTE PROCEDURE public.history(
--       'statushistory',
--       'created_at, order_id, api_status',
--       '$1.updated_at, $1.id, $1.api_status'
--   );

--   CREATE TRIGGER user_loginhistory
--   BEFORE UPDATE OF ip
--   ON public.users_user FOR EACH ROW
--   WHEN (OLD.ip IS DISTINCT FROM NEW.ip)
--   EXECUTE PROCEDURE public.history(
--       'loginhistory',
--       'created_at, user_id, ip',
--       '$1.updated_at, $1.id, $1.last_login_ip'
--   );

  CREATE TRIGGER order_history
  BEFORE UPDATE OF api_status, status_current, status_pending
  ON public.orders_order FOR EACH ROW
  EXECUTE PROCEDURE public.history_hstore(
      'history',
      'created_at, order_id',
      '$1.updated_at, $1.id',
      '{api_status, status_current, status_pending}'
  );

  CREATE TRIGGER user_history
  BEFORE UPDATE OF username, email, pin_code, is_active, is_admin, contact_skype, contact_jabber, contact_icq, contact_site, comments, tier1_rate, tier2_rate
  ON public.users_user FOR EACH ROW
  EXECUTE PROCEDURE public.history_hstore(
      'history',
      'created_at, user_id',
      '$1.updated_at, $1.id',
      '{username, email, pin_code, is_active, is_admin, contact_skype, contact_jabber, contact_icq, contact_site, comments, tier1_rate, tier2_rate}'
  );

  CREATE INDEX huser_old_gin_idx ON users_userhistory USING GIN(old_values);
  CREATE INDEX huser_new_gin_idx ON users_userhistory USING GIN(new_values);

  CREATE INDEX horder_old_gin_idx ON orders_orderhistory USING GIN(old_values);
  CREATE INDEX horder_new_gin_idx ON orders_orderhistory USING GIN(new_values);

END$$;
