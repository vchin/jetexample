DO $$
BEGIN
  CREATE OR REPLACE FUNCTION public.order_reorder (
  )
  RETURNS trigger AS
  $body$
  DECLARE
    l_id int;
  BEGIN
    IF (TG_OP = 'INSERT') THEN
      -- NEW.reorder = COALESCE((SELECT MAX(created_at) FROM orders_order WHERE user_id=NEW.user_id AND sub_account_id IS NOT DISTINCT FROM NEW.sub_account_id) BETWEEN SYMMETRIC NEW.created_at AND NEW.created_at - interval '1 day', FALSE);
      IF (SELECT MAX(created_at) FROM orders_order WHERE user_id=NEW.user_id AND sub_account_id IS NOT DISTINCT FROM NEW.sub_account_id) BETWEEN SYMMETRIC NEW.created_at AND NEW.created_at - interval '1 day' THEN
        NEW.reorder=TRUE;
        UPDATE users_user u SET reorders_qty = reorders_qty + 1 WHERE u.id=NEW.user_id;
      ELSE
        NEW.reorder=FALSE;
      END IF;
      RETURN NEW;
    END IF;
  EXCEPTION
  WHEN unique_violation THEN
  --  statements;
  END;
  $body$
  LANGUAGE 'plpgsql'
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  COST 100;

  CREATE TRIGGER order_reorder
  BEFORE INSERT
  ON public.orders_order FOR EACH ROW
  EXECUTE PROCEDURE public.order_reorder();
END$$;