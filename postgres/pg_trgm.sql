CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE INDEX products_product_trgm_idx ON products_product
  USING gist ("name_en" public.gist_trgm_ops);
