DO $$
BEGIN
  CREATE OR REPLACE FUNCTION public.hll_join_text (
    strs text []
  )
  RETURNS public.hll AS
  $body$
  BEGIN
  RETURN hll_add_agg(hll_hash_text(k)) FROM UNNEST(strs) k;
  END;
  $body$
  LANGUAGE 'plpgsql'
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  COST 100;
END$$;
