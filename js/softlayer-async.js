var DEBUG_MODE = 2; // tracing, debug, error
var DEBUG_MODES = {
    tracing: 1,
    debug: 2,
    error: 3
}

function console_log() {
    var params = Array.prototype.slice.call(arguments);
    var debug_mode = DEBUG_MODES['tracing'];

    if (arguments.length && DEBUG_MODES[arguments[0]]) {

        debug_mode = DEBUG_MODES[arguments[0]];
        params = params.slice(1);
    }


    if (debug_mode >= DEBUG_MODE) {
        console.log.apply(console, params);
    }
}

window.g_tm = null;

function sc_ready_tick(callback, options) {
    options = options || {};
    var tick_num = options.tick_num || 3;
    var tick_delay = options.tick_delay || 2;
    var tick = tick_num;

    function _tm() {
        clearTimeout(g_tm);
        g_tm = setTimeout(function () {
            console_log('tick', tick)
            if (--tick <= 0) {
                tick = tick_num;
                callback();
            }
            else _tm();
        }, tick_delay)
    }

    $('input[type="radio"]').off('click.scrapy').on('click.scrapy', _tm);

    _tm();
}

function parse(splash) {

    var options = window.SCRAPY_OPTIONS || {};
    var default_settings = {tick_delay: 2};

    options = $.extend(true, {}, default_settings, options);

    var results = [];
    var tick_delay = options.tick_delay;

    console_log(tick_delay);

    function _parse_config(jq_item) {
        return {
            category: $.trim(jq_item.find('.category-name').text()),
            name: $.trim(jq_item.find('.item-name').text()),
            price: $.trim(jq_item.find('.item-price').text().replace(/[ \n\r] +/, ' '))

        }
    }

    function _item_populate() {
        var item = {
            config: [{category: 'totals-monthly', name: 'amount', price: $('#totals-monthly').text()}]
        };

        console_log($('#summary-item-datacenter').text(), ' totals', $('#totals-monthly').text());

        $('.summary-items .summary-item').not('style[display="none"]').each(function () {
            var jq_self = $(this);

            if (jq_self.css('display') != 'none') {
                item.config.push(_parse_config($(this)));
            }
        });

        return item;
    }

    var selectors = [
        function () {
            return {
                name: 'datacenter',
                jq_items: $('#subsections-accordion-datacenter .item-description').not(':hidden').find('input')
            };
        },
        function () {
            return {
                name: 'server',
                jq_items: $('#item-server-content .item-description').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'ram',
                jq_items: $('#item-ram-content .item-description').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'os group',
                jq_items: $('#item-os-content a')
            }
        },
        function () {
            return {
                name: 'os group item',
                jq_items: $('#item-os-content label').not(':hidden').find('input')
            };
        },
        function () {
            return {
                name: 'bandwidth group',
                jq_items: $('#item-bandwidth-content a')
            }
        },
        function () {
            return {
                name: 'bandwidth group item',
                jq_items: $('#item-bandwidth-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-port-speed-content group',
                jq_items: $('#item-port-speed-content a')
            }
        },
        function () {
            return {
                name: 'item-port-speed-content group item',
                jq_items: $('#item-port-speed-content label').not(':hidden').find('input')
            };
        },
        function () {
            return {
                name: 'item-sec-ip-addresses-content',
                jq_items: $('#item-sec-ip-addresses-content .item-description').not(':hidden').find('input')

            }
        },
        function () {
            return {
                name: 'item-pri-ipv6-addresses-content',
                jq_items: $('#item-pri-ipv6-addresses-content .item-description').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-static-ipv6-addresses-content',
                jq_items: $('#item-static-ipv6-addresses-content .item-description').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-os-addon-content block',
                jq_items: $('#item-os-addon-content a')
            }
        },
        function () {
            return {
                name: 'item-os-addon-content block item',
                jq_items: $('#item-os-addon-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-cdp-backup-content',
                jq_items: $('#item-cdp-backup-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-control-panel-content block',
                jq_items: $('#item-control-panel-content a')
            }
        },
        function () {
            return {
                name: 'item-control-panel-content block item',
                jq_items: $('#item-control-panel-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-database-content block',
                jq_items: $('#item-database-content a')
            }
        },
        function () {
            return {
                name: 'item-database-content block item',
                jq_items: $('#item-database-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-firewall-content block',
                jq_items: $('#item-firewall-content a')
            }
        },
        function () {
            return {
                name: 'item-firewall-content block item',
                jq_items: $('#item-firewall-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-av-spyware-protection-content',
                jq_items: $('#item-av-spyware-protection-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-intrusion-protection-content',
                jq_items: $('#item-intrusion-protection-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-monitoring-package-content',
                jq_items: $('#item-monitoring-package-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-evault-content',
                jq_items: $('#item-evault-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-evault-plugin-content',
                jq_items: $('#item-evault-plugin-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-monitoring-content',
                jq_items: $('#item-monitoring-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-response-content',
                jq_items: $('#item-response-content label').not(':hidden').find('input')
            }
        },
        function () {
            return {
                name: 'item-bc-insurance-content',
                jq_items: $('#item-bc-insurance-content label').not(':hidden').find('input')
            }
        },

    ];

    if (window.SCRAPY_DEBUG) {
        selectors = selectors.slice(0, window.SCRAPY_OPTIONS['slice']);
    }


    var uniq = {}

    function _uniq_check(data) {
        var jq_items = data['jq_items'],
                name = data['name'];

        if (!uniq[name]) {
            uniq[name] = {
                items: {},
                is_uniq: false
            };

            jq_items.each(function () {
                var text = $(this).closest('label').text();
                uniq[name]['items'][text] = text;
            })
        }
        else {
            if (!uniq[name]['is_uniq']) {
                jq_items.each(function () {
                    var text = $(this).closest('label').text();
                    if (!uniq[name]['items'][text]) {
                        uniq[name]['is_uniq'] = true;
                        uniq[name]['uniq'] = text;

                        return false;
                    }
                })
            }
        }
    }

    // wrap selectors to find immutable
    $.each(selectors, function (key, val) {
        //console.log()
        selectors[key] = function () {
            var data = val();
            _uniq_check(data);
            return data;
        };
    })

    var timeout = function () {
        var defer = Q.defer();

        sc_ready_tick(function () {
            defer.resolve()
        }, {tick_delay: tick_delay})

        return defer.promise;
    }

    function _sub_call(ndx) {
        var defer = Q.defer();
        Q().then(timeout).then(function () {
            var pr = Q();
            selectors[ndx]()['jq_items'].each(function (ii) {
                var self = $(this);
                pr = pr.then(timeout).then(function () {
                    console_log('clk ', self.attr('id'))
                    self.prop('checked', true).click();

                    if (ndx + 1 < selectors.length) {
                        return _sub_call(ndx + 1)
                    }
                })

                if (ndx == selectors.length - 1) {
                    pr = pr.then(timeout).then(function () {
                        var result = _item_populate();
                        results.push(result)
                        //console.log(result);
                        console_log('parsed>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                    })
                }

                if (window.SCRAPY_DEBUG) {
                    if (ii == (window.SCRAPY_OPTIONS['num_items'] - 1)) return false;
                }
            })

            pr.done(function () {
                defer.resolve();
            })
        })

        return defer.promise;

    }

    _sub_call(0).done(function () {
        if (splash) {
            splash.resume({results: results, uniq: uniq})
        }
        else {
            console_log(results);
        }
    });

};


if (!window.SCRAPY) {
    window.SCRAPY_DEBUG = true;
    window.SCRAPY_OPTIONS = {
        tick_delay: 2,
        slice: 100, // selectors
        num_items: 2 // items in selector
    };

    parse({
        resume: function (results) {
            var results_1 = results.results;
            console_log('debug', 'results', results);
            $.each(results['uniq'], function (key, val) {
                if (val.is_uniq) {
                    //console_log('debug', 'uniq:', key, ' val:', val.uniq, 'items', val.items)

                }
                else {
                    console_log('debug', 'not uniq:', key)
                }
                console_log('debug', '--------------------------')
            })

            return;
            var errs = 0;
            console_log('res1', results_1.length)
            window.SCRAPY_OPTIONS = {tick_delay: 300};

            parse({
                resume: function (results) {
                    console_log('res2', results.results.length)

                    $.each(results.results, function (ndx) {
                        $.each(this, function (ndx_2) {
                            $.each(this, function (ndx_3) {
                                var item_1 = results_1[ndx][ndx_2][ndx_3], item_2 = this;
                                $.each(['categiry', 'name', 'price'], function () {
                                    if (item_1[this] != item_2[this]) {
                                        errs++;
                                        console_log('error', item_1['this'], '!=', item_2[this]);
                                    }
                                })
                            })
                        })
                    })

                    console_log('errs:', errs);
                }
            })
        }
    });
}


