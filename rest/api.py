import coreapi
import requests
from urllib.parse import urlencode

import ujson
from django.shortcuts import render

# Create your views here.
from django.utils.safestring import mark_safe
from drf_haystack.filters import HaystackFilter
from drf_haystack.query import FilterQueryBuilder
from drf_haystack.serializers import HaystackSerializer, HaystackSerializerMeta, \
    HaystackSerializerMixin
from drf_haystack.viewsets import HaystackViewSet
from haystack.inputs import Raw
from haystack.query import SearchQuerySet
from rest_framework import permissions

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.filters import BaseFilterBackend
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.pagination import LimitOffsetPagination, _get_count
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from rest_framework.views import APIView

from urllib.parse import urlparse

from . import models as core_models
from . import search_indexes


class BaseFilter(BaseFilterBackend):
    action_map = {}

    def _validate(self, request, map_names=None):
        cleaned_data = {}
        params = request.query_params

        if map_names is None:
            map_names = ['default']

        for map_name in map_names:
            for field in self.action_map[map_name]:
                if not field.name in params:
                    continue

                val = params[field.name]

                if field.type == 'integer':
                    val = int(val)
                elif field.type == 'bool':
                    val = val == 'true'

                cleaned_data[field.name] = val

        return cleaned_data

    def get_schema_fields(self, view):
        if hasattr(view, 'action'):
            return self.action_map.get(view.action, [])
        return self.action_map.get('default', [])


class ExFilterQueryBuilder(FilterQueryBuilder):
    def build_query(self, **filters):
        filters.pop('fields', None)
        filters.pop('limit', None)
        filters.pop('offset', None)

        return super(ExFilterQueryBuilder, self).build_query(**filters)


class ExHaystackFilter(HaystackFilter):
    query_builder_class = ExFilterQueryBuilder


class ExLimitOffsetPagination(LimitOffsetPagination):
    max_limit = 20
    default_limit = 10


class CategoryList(ListAPIView):
    queryset = core_models.Category.objects.all()
    pagination_class = ExLimitOffsetPagination
    permission_classes = [permissions.AllowAny]

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            class Meta:
                model = core_models.Category
                fields = ['id', 'title']

        return _Serializer


class CategoryStoreList(ListAPIView):
    queryset = core_models.CategoryStore.objects.all()
    pagination_class = ExLimitOffsetPagination
    permission_classes = [permissions.AllowAny]

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            class Meta:
                model = core_models.CategoryStore
                fields = ['id', 'title']

        return _Serializer


class ControllerList(ListAPIView):
    queryset = core_models.Controller.objects.all()
    pagination_class = ExLimitOffsetPagination
    permission_classes = [permissions.AllowAny]

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            class Meta:
                model = core_models.Controller
                fields = ['id', 'title', 'buttons', 'joystick']

        return _Serializer


class SearchView(HaystackViewSet):
    '''
    *params in fields:*
        store_id, title, price, platform, pay_type,
        categories_store, store_ratio, download,
        download_min, published, version,
        developer, countries, logo,
        requirements, screenshots, video,
        in_app_prices, badges, original_data, status,
        description_short, description,
        description_short_en, description_en,
        description_original, description_original_en,
        app_type,
        category,
        game_position,
        priority,
        glasses,
        ctl_is_required,
        ctl_joystick,
        ctl_buttons,
        size

    *filter params:*
        text,
        size,
        store_id,
        title,
        title_en,
        price,
        platform,
        pay_type,
        categories_store,
        store_ratio,
        developer,
        priority,
        app_type,
        category,
        game_position,
        priority,
        glasses,
        ctl_is_required,
        ctl_joystick,
        ctl_buttons
    '''
    queryset = core_models.App.objects.all()
    permission_classes = [permissions.AllowAny]
    pagination_class = ExLimitOffsetPagination

    filter_backends = [ExHaystackFilter]
    document_uid_field = 'django_id'

    def get_queryset(self, index_models=[]):
        return SearchQuerySet().filter(
            status=core_models.App.T_APPROVE,
            is_published=1,
            is_archived=0
        ).order_by('-priority')

    def get_serializer_class(self):
        avail_fields = {'id', 'store_id', 'title', 'title_en', 'price',
                        'platform', 'pay_type',
                        'categories_store', 'store_ratio', 'download',
                        'download_min', 'published', 'version',
                        'developer', 'countries', 'logo',
                        'requirements', 'screenshots', 'video',
                        'in_app_prices', 'badges', 'original_data', 'status',
                        'size',
                        'description_short', 'description',
                        'description_en', 'description_short_en',

                        'description_original', 'description_original_en',

                        'app_type',
                        'category',
                        'game_position',
                        'priority',
                        'glasses',
                        'ctl_is_required',
                        'ctl_joystick',
                        'ctl_buttons'}

        result_fields = ['id']

        if 'fields' in self.request.query_params:
            for field in self.request.query_params['fields'].split(','):
                field = field.strip()
                if field in avail_fields:
                    result_fields.append(field)
        else:
            result_fields = list(avail_fields)

        class _ModelSerilizer(serializers.ModelSerializer):
            developer = serializers.SerializerMethodField()
            screenshots = serializers.SerializerMethodField()
            countries = serializers.SerializerMethodField()
            original_data = serializers.SerializerMethodField()
            download = serializers.SerializerMethodField()
            requirements = serializers.SerializerMethodField()
            price = serializers.SerializerMethodField()

            def get_developer(self, obj):
                if obj.developer:
                    return ujson.loads(obj.developer)
                return []

            def get_screenshots(self, obj):
                if obj.screenshots:
                    return ujson.loads(obj.screenshots)
                return []

            def get_countries(self, obj):
                if obj.countries:
                    return ujson.loads(obj.countries)
                return []

            def get_original_data(self, obj):
                if obj.original_data:
                    return ujson.loads(obj.original_data)
                return {}

            def get_price(self, obj):
                return obj.price.amount if obj and obj.price else 0

            def get_download(self, obj):
                if obj.download:
                    return ujson.loads(obj.download)
                return []

            def get_requirements(self, obj):
                if obj.requirements:
                    return ujson.loads(obj.requirements)
                return {}

            class Meta:
                model = core_models.App
                fields = result_fields

        # HaystackSerializerMixin,
        class _Serializer(HaystackSerializerMixin, _ModelSerilizer):
            # id = serializers.SerializerMethodField()
            #
            # def get_id(self, obj):
            #     return obj.pk

            class Meta(_ModelSerilizer.Meta):
                pass
                # The `index_classes` attribute is a list of which search indexes
                # we want to include in the search.
                # index_classes = [search_indexes.AppIndex]

                # fields = result_fields

        return _Serializer
        # class _Serializer(serializers.ModelSerializer):
        #     class Meta:
        #         model = core_models.App
        #         fields = ['title', 'size']
        #
        # return _Serializer


class AppleStoreFilterBackend(BaseFilter):
    action_map = {
        'entity_list': [
            coreapi.Field(
                name='media',
                location='query',
                required=True,
                type='string'
            )
        ],
        'list': [
            coreapi.Field(
                name='term',
                location='query',
                required=True,
                type='string'
            ),
            coreapi.Field(
                name='country',
                location='query',
                required=True,
                type='string'
            ),
            coreapi.Field(
                name='media',
                location='query',
                required=False,
                type='string'
            ),
            coreapi.Field(
                name='entity',
                location='query',
                required=False,
                type='string'
            ),
        ]
    }


class FltoolzPagination(LimitOffsetPagination):
    max_limit = 100
    default_limit = 20

    def paginate_queryset(self, queryset, request, view=None):
        self.limit = self.get_limit(request)
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.count = _get_count(queryset)
        self.request = request
        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True

        if self.count == 0 or self.offset > self.count:
            return []

        if isinstance(queryset, SearchQuerySet):
            return queryset.load_all()[self.offset:self.offset + self.limit]
            # return [item.get_additional_fields() for item in
            #         queryset[self.offset:self.offset + self.limit]]

        return list(queryset[self.offset:self.offset + self.limit])


#
#
# class AdListView(ListAPIView):
#     permission_classes = [permissions.IsAuthenticated]
#     pagination_class = FltoolzPagination
#     queryset = core_models.Ad.objects.order_by('-id')
#
#     def get_serializer_class(self):
#         class _Serializer(serializers.ModelSerializer):
#             class Meta:
#                 model = core_models.Ad
#                 fields = ['site', 'url']
#                 read_only_fields = ['site', 'url']
#
#         return _Serializer




class AdSearchViewFilter(BaseFilter):
    action_map = {
        'default': [
            coreapi.Field(
                name='query',
                location='query',
                required=False,
                type='string'
            )
        ]
    }

    def filter_queryset(self, request, queryset, view):
        cleaned_data = self._validate(request)
        view.filter_cleaned_data = cleaned_data

        sqs = queryset.order_by(
            cleaned_data.get('order_by', '-date_time')
        )

        try:
            query = ujson.loads(cleaned_data['query'])
        except Exception as e:
            if cleaned_data.get('query', ''):
                query = [{'text': cleaned_data.get('query', '')}]
            else:
                query = ''

        if query:
            if isinstance(query, dict):
                query = [query]

            valid_fields = ['text',
                            'price__gt',
                            'price__gte',
                            'job_type__in']

            for q in query:
                escaped_query = {}
                for key in valid_fields:
                    if key in q:
                        escaped_query[key] = q[key]

                if 'text' in escaped_query:
                    escaped_query['text'] = Raw(escaped_query['text'])

                if len(escaped_query):
                    sqs = sqs.filter_or(**escaped_query)

                if core_models.Ad.JOB_CONTRACT in escaped_query.get(
                        'job_type__in', []):
                    escaped_query['job_type__in'] = [
                        core_models.Ad.JOB_CONTRACT]
                    escaped_query.pop('price__gte', None)
                    sqs = sqs.filter_or(**escaped_query)

        return sqs.all()
