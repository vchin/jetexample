class ExLimitOffsetPagination(LimitOffsetPagination):
    max_limit = 20
    default_limit = 10


def _paginate_cls(i_max_limit=20, i_default_limit=10):
    class _LimitOffsetPagination(LimitOffsetPagination):
        max_limit = i_max_limit
        default_limit = i_default_limit

    return _LimitOffsetPagination


class BaseFilter(BaseFilterBackend):
    action_map = {}

    def get_schema_fields(self, view):
        return self.action_map.get(view.action, [])


class PetViewSetBackend(BaseFilter):
    action_map = {
        'list': [
            coreapi.Field(
                name='pet',
                location='path',
                required=True,
                type='string'
            )
        ]
    }

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(pet_id=request.query_params.get('pet'))


class BaseExViewSet(viewsets.ViewSet):
    # filter_backends = [PetViewSetBackend]
    permission_classes = [permissions.IsAuthenticated]
    serializer_cls = False

    def list(self, request, *args, **kwargs):
        cls = self.serializer_cls
        model = cls.Meta.model
        qset = model.objects.filter(
            user=request.user, pet_id=kwargs['pet_id'])

        return Response({'results': cls(qset, many=True).data})

    @list_route(['post'])
    def list_create_update(self, request, *args, **kwargs):
        pet = get_object_or_404(core_models.Pet,
            pk=kwargs.get('pet_id'),
            user=request.user)

        ids = {}
        cls = self.serializer_cls
        model = cls.Meta.model
        queryset = model.objects.filter(
            user=request.user, pet=pet)
        instances = common_utils.qset_to_dict(queryset)

        for item in request.data:
            if 'id' in item:
                ids[item['id']] = item['id']

        if len(ids) or not len(request.data):
            queryset.exclude(id__in=ids.keys()).delete()

        for item in request.data:
            instance = instances.get(item.get('id', None), None)
            file = item.get('file', None)

            if instance and hasattr(instance, 'file') and instance.file:
                if not file or file.startswith('/media'):
                    item['file'] = instance.file

            srz = cls(instance=instance, data=item)
            srz.is_valid(raise_exception=True)
            srz.save(user=request.user, pet_id=pet.pk)

        return self.list(request=request, pet_id=pet.pk)


class PetViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = ExLimitOffsetPagination

    def get_queryset(self):
        return core_models.Pet.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            def validate(self, attrs):
                if attrs.get('price_bw') and attrs['price_bw'] < attrs[
                    'price_nbw']:
                    raise serializers.ValidationError(
                        {'price_bw': [_('Price must be higher')]})

                return attrs

            class Meta:
                model = core_models.Pet
                # TODO: add fields
                exclude = ['user', 'order', 'created_at', 'updated_at', 'state']

        return _Serializer


class Base64FileField(serializers.FileField):
    def to_internal_value(self, data):
        if isinstance(data, str) and 'base64' in data[0:100]:
            # base64 encoded image - decode
            # our custom format file:1.jpg;data:
            format, filestr = data.split(
                ';base64,')  # format ~= file:;Mime type;encoding, b64code
            filename = format.split(';')[0].split(':')[-1]  # guess file name

            data = ContentFile(base64.b64decode(filestr), name=filename)

        return super(Base64FileField, self).to_internal_value(data)


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str) and data.startswith('data:image'):
            # base64 encoded image - decode
            format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            ext = format.split('/')[-1]  # guess file extension

            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        return super(Base64ImageField, self).to_internal_value(data)


class PetImageViewSet(viewsets.ModelViewSet):
    """
    Image
    """
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = _paginate_cls(30, 30)

    # filter_backends = [PetViewSetBackend]

    def filter_queryset(self, queryset):
        if self.action == 'list':
            return super(PetImageViewSet, self).filter_queryset(queryset)
        return queryset

    @list_route(['get', 'post'])
    def list_create_update(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        instances = common_utils.qset_to_dict(queryset)
        ids = {}
        for item in request.data['items']:
            if 'id' in item:
                ids[item['id']] = item['id']

        if len(ids) or not len(request.data['items']):
            queryset.exclude(id__in=ids.keys()).delete()

        for item in request.data['items']:
            instance = instances.get(item.get('id', None), None)
            if instance and not item['image'] or item['image'].startswith(
                    'http'):
                item['image'] = instance.image if instance.image else None

            serializer = self.get_serializer(instance=instance,
                data=item)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        return core_models.PetImage.objects.filter(
            user=self.request.user, pet_id=self.kwargs['pet_id'])

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            image = Base64ImageField()
            parser_classes = [parsers.FormParser, parsers.MultiPartParser]

            class Meta:
                model = core_models.PetImage
                fields = ['id', 'pet', 'image', 'name', 'created', 'is_cover']

        return _Serializer


#####  HEALTH   ########
class PetWeightViewSet(viewsets.ModelViewSet):
    """
    Health - weight

    retrieve:
    Health - return the given weight.

    list:
    Health - return a list of all the existing weights.

    create:
    Health - create a new weight instance.

    delete:
    Health - delete weight
    """
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = ExLimitOffsetPagination

    # filter_backends = [PetImageViewSetBackend]

    def get_queryset(self):
        return core_models.PetWeight.objects.filter(
            user=self.request.user,
            # pet=self.request.query_params.get('pet'),
        )

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            class Meta:
                model = core_models.PetWeight
                exclude = ['order', 'created_at', 'updated_at', 'state']

        return _Serializer


class PetHealthSerializer(serializers.ModelSerializer):
    class Meta:
        model = core_models.PetHealth
        fields = ['id', 'is_castrated', 'is_flea_safe', 'blood_type']
########################


class Filter(object):
    def __init__(self, queryset, query_args):
        self.queryset = queryset
        self.query_args = query_args

    def content_name(self):
        """
        Поиск строки в названиях, описаниях и тегах треков
        """
        name = self.query_args.get('name')
        if name:
            # Case-insensitive containment test
            self.queryset = self.queryset.filter(
                Q(name__icontains=name[0]) | Q(desc__icontains=name[0]) | Q(
                    metadata__value__icontains=name[0])
            )

    def content_price_min(self):
        """
        Поиск по мин. цене контента
        """
        price = self.query_args.get('priceMin')
        if price:
            # Greater than or equal to
            self.queryset = self.queryset.filter(price__gte=int(price[0]))

    def content_price_max(self):
        """
        Поиск по макс. цене контента
        """
        price = self.query_args.get('priceMax')
        if price:
            # Less than or equal to
            self.queryset = self.queryset.filter(price__lte=int(price[0]))

    def audio_duration_min(self):
        """
        Поиск по мин. продолжительности трека
        """
        duration = self.query_args.get('durationMin')
        if duration:
            # Greater than or equal to
            self.queryset = self.queryset.filter(
                audiocontent__duration__gte=int(duration[0]))

    def audio_duration_max(self):
        """
        Поиск по макс. продолжительности трека
        """
        duration = self.query_args.get('durationMax')
        if duration:
            # Less than or equal to
            self.queryset = self.queryset.filter(
                audiocontent__duration__lte=int(duration[0]))

    def metadata(self, metadata_type, key):
        """
        Поиск по метаданным (используется объединение по "ИЛИ")
        """
        if key in self.query_args:
            self.queryset = self.queryset.filter(
                metadata__type=metadata_type,
                metadata__value__in=self.query_args[key]
            )

    def ordering(self, prefix=''):
        """
        Сортировка результатов
        """
        fields = self.query_args.get('ordering')

        if fields and self.queryset is not None:
            # Добавить префикс, чтобы обратиться к другой модели
            for i in range(len(fields)):
                if fields[i].startswith('-'):
                    fields[i] = '-' + prefix + fields[i].lstrip('-')
                else:
                    fields[i] = prefix + fields[i]

            self.queryset = self.queryset.order_by(*fields)

    def limiting(self):
        """
        Ограничение количества выдаваемых результатов
        """

        begin = abs(int(self.query_args.get('begin')[
            0] if 'begin' in self.query_args else 0))
        end = abs(int(
            self.query_args.get('end')[0] if 'end' in self.query_args else 30))

        count = self.queryset.count()  # Внимание: дополнительный запрос к БД!

        if end > count: end = count
        if begin > end: begin = end  # Начальный индекс всегда меньше или равен конечному
        if end - begin > 30: end = begin + 30  # Выдать можно не больше N результатов

        self.queryset = self.queryset[begin:end]
        return count  # Общее количество результатов

    def data(self, request):
        """
        Вернуть результат фильтрации в виде списка объектов Content (сериализованных)
        """
        # if self.queryset is None:  # На пустой запрос нужно тоже возвращать данные
        #     return []

        # Сортировка выборки
        self.ordering()

        # Ограничение выборки некоторым количеством результатов
        count = self.limiting()

        # Сериализация результата
        serializer = ContentSerializer(self.queryset, many=True,
            context={'request': request})
        return dict(count=count, result=serializer.data)

    def audio(self, request):
        """
        Вернуть результат фильтрации в виде списка объектов Audio (сериализованных)
        """
        # if self.queryset is None:  # На пустой запрос нужно тоже возвращать данные
        #     return []

        # Ограничение выборки некоторым количеством результатов
        count = self.limiting()

        # Выбор аудио контента
        ids = self.queryset.values_list('id', flat=True)
        if not ids:
            return dict(count=0, result=[])

        self.queryset = AudioContent.public.filter(pk__in=ids)

        # Сортировка выборки
        self.ordering('content__')

        # Сериализация результата
        serializer = AudioContentSerializer(self.queryset, many=True,
            context={'request': request})
        return dict(count=count, result=serializer.data)


class ElasticFilter(Filter):
    def metadata(self, metadata_type, key):
        """
        Поиск по метаданным (используется объединение по "ИЛИ")
        """
        if key in self.query_args:
            items = [u'{}__{}'.format(metadata_type, val) for val in
                     self.query_args[key]]

            self.queryset = self.queryset.filter('terms',
                **{'meta_type_val': items})

    def field(self, key, fun):
        if key in self.query_args:
            val = fun(self.query_args[key][0])
            if not val is None:
                self.queryset = self.queryset.filter('term', **{
                    key: self.query_args[key][0]})

    def content_name(self):
        """
        Поиск строки в названиях, описаниях и тегах треков
        """
        name = self.query_args.get('name')
        if name:
            # Case-insensitive containment test
            self.queryset = self.queryset.query(
                'multi_match',
                query=name[0],
                fields=['name', 'text'],
                fuzziness='AUTO',
                operator='AND'
            )

    def audio(self, request):
        """
        Вернуть результат фильтрации в виде списка объектов Audio (сериализованных)
        """
        # if self.queryset is None:  # На пустой запрос нужно тоже возвращать данные
        #     return []

        # Сортировка выборки из elastic
        self.ordering('')

        # Ограничение выборки некоторым количеством результатов
        count = self.limiting()

        # Выбор аудио контента

        ids = [item.django_id for item in self.queryset]
        if not ids:
            return dict(count=0, result=[])

        self.queryset = AudioContent.public.filter(pk__in=ids)

        # Сортировка выборки из db
        self.ordering('content__')

        # Сериализация результата
        serializer = AudioContentSerializer(self.queryset, many=True,
            context={'request': request})
        return dict(count=count, result=serializer.data)

    def content_price_min(self):
        """
        Поиск по мин. цене контента
        """
        price = self.query_args.get('priceMin')
        if price:
            # Greater than or equal to
            self.queryset = self.queryset.filter('range', price={
                'gte': int(price[0])})

    def content_price_max(self):
        """
        Поиск по макс. цене контента
        """
        price = self.query_args.get('priceMax')
        if price:
            # Less than or equal to
            self.queryset = self.queryset.filter('range', price={
                'lte': int(price[0])})

    def audio_duration_min(self):
        """
        Поиск по мин. продолжительности трека
        """
        duration = self.query_args.get('durationMin')
        if duration:
            # Greater than or equal to
            self.queryset = self.queryset.filter('range', duration={
                'gte': int(duration[0])})

    def audio_duration_max(self):
        """
        Поиск по макс. продолжительности трека
        """
        duration = self.query_args.get('durationMax')
        if duration:
            # Less than or equal to
            self.queryset = self.queryset.filter('range', duration={
                'lte': int(duration[0])})

    def ordering(self, prefix=''):
        """
        Сортировка результатов
        """
        fields = self.query_args.get('ordering')

        if fields and self.queryset is not None:
            fields = fields[:]
            # if elastic then sort, else order-by for queryset
            method = 'order_by' if prefix else 'sort'

            # Добавить префикс, чтобы обратиться к другой модели
            for i in range(len(fields)):
                if not prefix and fields[i] == 'name':
                    fields[i] = 'name_sort'

                if fields[i].startswith('-'):
                    fields[i] = '-' + prefix + fields[i].lstrip('-')
                else:
                    fields[i] = prefix + fields[i]

            self.queryset = getattr(self.queryset, method)(*fields)


class SearchContent(APIView):
    """
    Поиск контента по атрибутам и метаданным, с фильтрацией и ограничением выборки.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        query_args = dict(request.GET.iterlists())
        # queryset = Content.public.all()  # Только среди опубликованных
        #
        # filters = Filter(queryset=queryset, query_args=query_args)

        queryset = Search(using=connections['default'].get_backend().conn,
            index='tunestock').filter('term',
            django_ct=u'content.content')

        filters = ElasticFilter(queryset=queryset, query_args=query_args)

        # Поиск по метаданным
        filters.metadata(Metadata.CATEGORY_ID, 'category')
        filters.metadata(Metadata.GENRE_ID, 'genre')
        filters.metadata(Metadata.VOCALS_ID, 'vocals')
        filters.metadata(Metadata.BEATS_PER_MINUTE_ID, 'bpm')
        filters.metadata(Metadata.TAG_ID, 'tag')

        # Поиск по полям контента
        filters.content_name()
        filters.content_price_min()
        filters.content_price_max()

        # Поиск по полям аудио
        filters.audio_duration_min()
        filters.audio_duration_max()

        # Фильтрация по полю, если присутствует
        filters.field('position', lambda v: 1 if int(v) == 1 else None)
        filters.field('seasonal', lambda v: True if int(v) == 1 else None)

        return Response(filters.audio(request), status=status.HTTP_200_OK)


class SearchContentStat(APIView):
    """
    Выборка топовых авторов
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        limit = 30
        tp = request.GET.get('type', None)

        if not tp:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        ids = []
        order_by = '-content__created'
        queryset = ContentStat.objects.all()

        if tp == 'created':
            ids = queryset.order_by('-content_created__created').values_list(
                'content_created_id', flat=True)[0:limit]

        elif tp == 'sales':
            ids = queryset.order_by('-content_sales__sales').values_list(
                'content_sales_id', flat=True)[0:limit]
            order_by = '-content__sales'
        elif tp == 'free':
            ids = queryset.filter(content_free__price=0).values_list(
                'content_free_id', flat=True)[0:limit]

        audio_queryset = AudioContent.public.filter(pk__in=ids).order_by(
            order_by)

        serializer = AudioContentSerializer(
            audio_queryset,
            many=True,
            context={'request': request})

        result = dict(count=audio_queryset.count(), result=serializer.data)

        return Response(result, status=status.HTTP_200_OK)